## Hilde
### Features
#### Defined input and output files
- `trajectory.yaml`
- `settings.in`
	- `[phonopy]`
	- `[phonopy_output]`
- `fireworks.in`

#### Main scripts
- `hilde input`: e.g. `hilde_input phonopy` creates default `settings.in` for phonopy
- `hilde info`: inform about the calculation (status)
- `hilde run`: run the task
- `hilde output`: run postprocessing

### Methodology
`hilde_run`

- general aims task
	- provide `xc` and `k_grid` in `settings.in`
	- `[control]`
- Relaxation
	- use `aims`
	- constrained and free
	- `[relax]`
	- `[relax_output]`
- ? Convergence
		- `[convergence]`: `xc`, `basisset`, `k_grid: automatic`
- Phonons
	- `[phonopy]`: `supercell_matrix`, `displacement`, ...
	- `[phonopy_output]`: `k_path`, `dos_range`, `q_mesh`, ...
- Stat. Sampling
	- `[statistical_sampling]`: `random_seed`, `temperature`, `force_constants`, `deterministic`

- Anharmonicity Score
	- function that takes `force_constants` + `trajectory`
- MD:
	- `[md]`: `temperature`, `algorithm`, ...

- Supercell setup
	- `make_supercell`
	- problem of different supercells
		- just use the phonopy one? Just one convention!
		- then one could provide `geometry.in.primitive` and `supercell_matrix` instead of `geometry.in.supercell`
	- `--phonopy`: number of displacements



### High Troughput functionality
- Fireworks support
- Phonons
	- Force constants remapping _without_ TDEP?
- Stat. Sampling
- MD
- Anharmonicity Score

### Nice to Have
- `phonopy-FHIaims` compatibility layer	
- `phono3py` support


### Release
- Paper draft and release until Handson Barcelona


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIwMDcyMTMwOTFdfQ==
-->