Umwelt und Nachhaltigkeit am FHI
===
### Reisen
- wie emissionsarmes Reisen fördern?
	- wenn Zug teurer als Flugzeug
	- längere Anreise, Frage der Arbeitszeit
- Deckelung von Flugreisen
	- ggf als softere Variante, selbstauferlegt etc
- Wie sieht das bisher aus mit CO2-Kompensation?

### Ressourcen
- Recycling-Papier
- Minimierung von Plastikartikeln im Materiallager
	- Schnellhefter
	- Lineal

### TH-Abteilung
- Awareness für Stromverbrauch der Cluster

### Umsetzung
- Externe Experten?
    - https://www.akzente.de/ (Random Beispiel)

###### tags: `FHI`, `Umwelt`
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzNDQyMjUwMywtNDg4Njg0MDIxLDE4MD
czMzU5MTJdfQ==
-->